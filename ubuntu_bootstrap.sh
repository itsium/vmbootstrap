#!/bin/sh

# funciotns

promptyn () {
    while true; do
        read -p "$1 " yn
        case $yn in
            [Yy]* ) return 0;;
            [Nn]* ) return 1;;
            * ) echo "Please answer y or n.";;
        esac
    done
}

# main

if [ `id -u` != "0" ]; then
  echo "[+] You have to run this script with root privilege !"
  exit 1
fi
echo "[+] Update and upgrade system !"
aptitude update
aptitude -y safe-upgrade
echo "[+] Install basic tools !"
apt-get -y install ntp ntpdate emacs24-nox curl
echo "[+] Time configure !"
dpkg-reconfigure tzdata

echo "[+] remove old ruby"
apt-get -y remove "^libruby*$" "^ruby*$"
echo "[+] rails dep !"
aptitude install -y ruby2.0-dev build-essential wget libruby2.0 rubygems
apt-get -y build-dep ruby2.0 rails


apt-get -y install libmysql++-dev libsqlite3-dev
apt-get -y install imagemagick
echo 'gem: --no-rdoc --no-ri' >> ~/.gemrc
gem update --system
gem install ohai chef