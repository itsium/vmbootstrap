# --- Install nginx ---

# First, remove unneeded packages
%w{ nginx nginx-light nginx-full nginx-extras }.each do |pkg|
  package pkg do
    action :remove
  end
end

# Install nginx-common (contains the init-scripts) and packages needed for compilation
%w{ nginx-common build-essential libcurl4-openssl-dev libssl-dev zlib1g-dev libpcre3-dev }.each do |pkg|
  package pkg
end

remote_file	'download nginx' do
	action :create_if_missing
	owner 'root'
	group 'root'
	mode '0644'
	path "/usr/src/nginx-#{node[:nginx][:version]}.tar.gz"
	source "http://nginx.org/download/nginx-#{node[:nginx][:version]}.tar.gz"
	not_if do
		File.exist? "/usr/src/nginx-#{node[:nginx][:version]}.tar.gz"
	end
end

execute 'extract nginx' do
	command "tar xvfz nginx-#{node[:nginx][:version]}.tar.gz"
	cwd '/usr/src'
	not_if do
		File.directory? "/usr/src/nginx-#{node[:nginx][:version]}"
	end
end

ruby_block "edit server signature" do
  src_file = "/usr/src/nginx-#{node[:nginx][:version]}/src/http/ngx_http_header_filter_module.c"

  block do
    rc = Chef::Util::FileEdit.new(src_file)
    rc.search_file_replace_line(/static char ngx_http_server_string.*$/,
       "static char ngx_http_server_string[] = \"#{node[:nginx]['server_signature']}\" CRLF;")
    rc.search_file_replace_line(/static char ngx_http_server_full_string.*$/,
       "static char ngx_http_server_full_string[] = \"#{node[:nginx]['server_signature']}\" CRLF;")
    rc.write_file
  end
  not_if do
  	node[:nginx]['server_signature'].nil? || File.open(src_file).read().include?(node[:nginx]['server_signature'])
  end
end

# -without-http_uwsgi_module --without-http_fastcgi_module && make && make install



configure_flags = [
  "--prefix=/opt/nginx-#{node[:nginx][:version]}",
  "--user=#{node[:nginx][:user]}",
  "--group=#{node[:nginx][:user]}",
  "--without-http_scgi_module",
  "--without-http_uwsgi_module",
  "--without-http_fastcgi_module",
  "--conf-path=/etc/nginx/nginx.conf",
  "--with-http_ssl_module"
]


execute "compile nginx" do
  user      "root"
  group     "root"
  cwd       "/usr/src/nginx-#{node[:nginx][:version]}"
  command   %{./configure #{configure_flags.join(' ')} && make && make install}
  creates   "/opt/nginx-#{node[:nginx][:version]}/sbin/nginx"
  not_if do
    File.exists?("/opt/nginx-#{node[:nginx][:version]}/sbin/nginx")
  end
end


# Setup nginx environment
link '/usr/sbin/nginx' do
  to "/opt/nginx-#{node[:nginx][:version]}/sbin/nginx"
end

link '/etc/nginx/logs' do
  to '/var/log/nginx'
end

# Configuration files

template '/etc/nginx/nginx.conf' do
  owner 'root'
  group 'root'
  mode '0644'
  source 'nginx.conf.erb'
  notifies :reload, "service[nginx]"
end

template "/etc/init/nginx.conf" do
  owner "root"
  group "root"
  mode "0755"
  backup false
  source 'nginx.upstart.conf.erb'
  # notifies :restart, resources(:service => "nginx"), :delayed
end

ruby_block "patch nginx default site conf " do
  src_file = "/etc/nginx/sites-available/default"

  block do
    rc = Chef::Util::FileEdit.new(src_file)
    rc.search_file_replace_line(/listen \[::\]:80 default_server ipv6only=on;/,
       "#REPLACED patch nginx default site conf REPLACED listen [::]:80 default_server ipv6only=on;")
    rc.search_file_replace_line(/allow ::1;/,
       "# allow ::1;")
    rc.write_file
  end
  not_if do
    File.open(src_file).read().include?("#REPLACED patch nginx default site conf REPLACED")
  end
end


service "nginx" do
  supports :status => true, :restart => true, :reload => true
  action [ :enable, :start ]
end

# service "nginx" do
#   supports  :restart => true, :reload => true, :status => true
#   action    [ :enable, :start ]
# end
