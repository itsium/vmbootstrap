#!/bin/sh

# Vars
projname="toto"
deployer="www-data"
shell_user="ubuntu"
######################
proj_path="/srv/www/"$projname

if [ `id -u` != "0" ]; then
    echo "[+] You have to run this script with root privilege !"
    exit 1
fi

mkdir -p $proj_path
chown -R $shell_user:$deployer $proj_path
chmod -R g+w $proj_path

cd /etc/init/
wget https://raw.githubusercontent.com/puma/puma/master/tools/jungle/upstart/puma-manager.conf
wget https://raw.githubusercontent.com/puma/puma/master/tools/jungle/upstart/puma.conf
echo $proj_path"/current" >> /etc/puma.conf



# Local Tuto
# 1. add gems and bundle install
# gem 'capistrano', '~> 3.2.0'
# gem 'capistrano-rails', '~> 1.1'
# gem 'capistrano-puma', require: false
#
# 2. run
# bundle exec cap install
#
# 3. set application, repo_url, deploy_to in config/deploy.rb
#
# 4. uncomment branch and linked_dirs lines in config/deploy.rb
#
# 5. add following line in config/deploy.rb
#     after "deploy:restart",        "puma:restart"
#
# 6. set proper value in config/deploy/production.rb
#
# 7. add follow line to Capfile comment out assets if don't need
# require 'capistrano/bundler' # Rails needs Bundler, right?
# require 'capistrano/rails/assets'
# require 'capistrano/rails/migrations'
# require 'capistrano/puma'
#
# 8. create puma.rb
#
# rails_env = ENV['RAILS_ENV'] || 'development'

# threads 4,4

# bind  "unix:///srv/www/sn_invit/current/tmp/sn_invit.sock"
# pidfile "/srv/www/sn_invit/current/tmp/pids/puma.pid"
# state_path "/srv/www/sn_invit/shared/tmp/sockets/puma.state"

# activate_control_app
